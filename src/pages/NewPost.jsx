import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { addPost } from '../actions/postActions';

export default function NewPost() {
    const [id, setId] = useState(1);
    const [title, setTitle] = useState('');
    const [author, setAuthor] = useState('');
    const [text, setText] = useState('');

    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        addPost(
            {
                id: 0,
                title: title,
                author: author,
                text: text
            }
        );
        history.push('/posts');
    }

    const handleChange = (e) => {
        let name = e.target.name;
        let val = e.target.value;
        switch (name) {
            case 'id':
                setId(val)
                break;
            case 'title':
                setTitle(val)
                break;
            case 'author':
                setAuthor(val)
                break;
            case 'text':
                setText(val)
                break;
            default:
                break;
        }
    }

    return (
        <>
            <h2 className="text-warning">Bootstrap modals on the Posts page provide better experience</h2>
            <form onSubmit={handleSubmit}>
                {/* <div className="form-group row">
                <label className="col-md-2">Id</label>
                <input type="number" className="from-control col-md-10" name="id" value={id} onChange={handleChange} />
                </div> */}
                <div className="form-group row">
                    <label className="col-md-2">Title</label>
                    <input type="text" className="from-control col-md-10" name="title" value={title} onChange={handleChange} />
                </div>
                <div className="form-group row">
                    <label className="col-md-2">Author</label>
                    <input type="text" className="from-control col-md-10" name="author" value={author} onChange={handleChange} />
                </div>
                <div className="form-group row">
                    <label className="col-md-2">Comment</label>
                    <textarea className="form-control col-md-10" name="text" value={text} onChange={handleChange}></textarea>
                </div>
                <button className="btn btn-primary">Post</button>

            </form>
        </>
    )
}

