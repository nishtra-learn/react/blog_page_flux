import React from 'react';

export default function Main() {
    return (
        <div className="jumbotron">
            <div className="display-4">Welcome to the Flux blog</div>
            <div className="lead">This is a Main page</div>
        </div>
    )
}