import React, { useState, useEffect } from 'react';
import data from '../db.json';
import Posts from './Posts';
import postStore from '../stores/postStore';
import { getPosts, deletePost, updatePost, addPost } from '../actions/postActions'
import { Modal } from "react-bootstrap";

export default function PostList() {
    // No Flux
    // const [posts, setPosts] = useState([]);
    // useEffect(() => {
    //     setPosts(data['posts']);
    //     console.log('Request to API/file');
    // }, [])

    // post properties
    //let id = -1;
    const [id, setId] = useState(-1);
    const [title, setTitle] = useState('');
    const [author, setAuthor] = useState('');
    const [text, setText] = useState('');

    const handleModalFieldsChange = (e) => {
        let name = e.target.name;
        let val = e.target.value;
        switch (name) {
            case 'title':
                setTitle(val)
                break;
            case 'author':
                setAuthor(val)
                break;
            case 'text':
                setText(val)
                break;
            default:
                break;
        }
    }

    // react-bootstrap modal handlers
    const [postModalShow, setPostModalShow] = useState(false);
    const handlePostModalShow = () => { setPostModalShow(true) };
    const handlePostModalClose = () => { setPostModalShow(false); setId(-1); };

    const [confirmDeleteModalShow, setConfirmDeleteModalShow] = useState(false);
    const handleConfirmDeleteModalShow = () => { setConfirmDeleteModalShow(true) };
    const handleConfirmDeleteModalClose = () => { setConfirmDeleteModalShow(false); setId(-1); };

    const showNewPostModal = () => {
        setId(0);
        setTitle('');
        setAuthor('');
        setText('');
        handlePostModalShow();
    }

    const showPostEditModal = (post) => {
        console.log(post);
        setId(parseInt(post.id));
        setTitle(post.title);
        setAuthor(post.author);
        setText(post.text);
        handlePostModalShow();
    }

    const confirmDelete = (postId) => {
        setId(parseInt(postId))
        console.log(id);
        handleConfirmDeleteModalShow();
    }

    // Flux
    const [posts, setPosts] = useState(postStore.getPosts());
    useEffect(() => {
        postStore.addChangeListener(onPostCollectionChange);
        if (postStore.getPosts().length === 0)
            getPosts();

        return () => postStore.removeChangeListener(onPostCollectionChange);
    }, [])

    const onPostCollectionChange = () => {
        console.log('Request to API/file');
        setPosts(postStore.getPosts());
    }

    const handleDeletePost = () => {
        console.log(id);
        if (id > 0)
            deletePost(id);
        handleConfirmDeleteModalClose();
    }

    const handlePostSend = () => {
        if (id === -1)
            throw new Error('Message id = -1. The message canbot be sent');
        if (title.length < 1 || author.length < 1 || text.length < 1)
            return;

        let post = { id, title, author, text };
        if (id === 0) {
            addPost(post);
        }
        else {
            updatePost(post);
        }

        handlePostModalClose();
    }

    return (
        <>
            <h2>Posts</h2>
            <button className="btn btn-primary" onClick={showNewPostModal}>New post</button>
            <Posts data={posts} onPostDelete={confirmDelete} onPostEdit={showPostEditModal}></Posts>

            <Modal
                size="lg"
                centered
                show={postModalShow}
                onHide={handlePostModalClose}
                backdrop="static"
                keyboard={false}>
                <Modal.Body>
                    <div className="container">
                        <div className="form-group row">
                            <label className="col-md-2">Title</label>
                            <input type="text" className="from-control col-md-10" name="title" value={title} onChange={handleModalFieldsChange} />
                        </div>
                        <div className="form-group row">
                            <label className="col-md-2">Author</label>
                            <input type="text" className="from-control col-md-10" name="author" value={author} onChange={handleModalFieldsChange} />
                        </div>
                        <div className="form-group row">
                            <label className="col-md-2">Comment</label>
                            <textarea className="form-control col-md-10" name="text" value={text} onChange={handleModalFieldsChange}></textarea>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button className="btn btn-secondary" onClick={handlePostModalClose}>Close</button>
                    <button className="btn btn-primary" onClick={handlePostSend}>Send</button>
                </Modal.Footer>
            </Modal>

            <Modal
                centered
                show={confirmDeleteModalShow}
                onHide={handleConfirmDeleteModalClose}
                backdrop="static"
                keyboard={false}>
                <Modal.Body>
                    <div className="container">
                        Are you sure you want to delete this post?
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button className="btn btn-danger" onClick={handleDeletePost}>Delete</button>
                    <button className="btn btn-secondary" onClick={handleConfirmDeleteModalClose}>Close</button>
                </Modal.Footer>
            </Modal>
        </>
    )
}