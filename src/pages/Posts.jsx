import React from 'react';

export default function Posts(props) {
    const posts = props.data;
    const handlePostDelete = (e) => {
        let postElement = e.target.parentElement.parentElement;
        let id = postElement.dataset.id;
        props.onPostDelete(id)
    };
    const handlePostEdit = (e) => {
        let postElement = e.target.parentElement.parentElement;
        let id = postElement.dataset.id;
        let title = postElement.querySelector('.post-title').textContent;
        let author = postElement.querySelector('.post-author').textContent;
        let text = postElement.querySelector('.post-text').textContent;
        props.onPostEdit({ id, title, author, text });
    }

    return (
        <>
            {
                posts.map(post =>
                    <div className="card mt-4 post" key={post.id} data-id={post.id}>
                        <div className="card-body">
                            <h5 className="card-title post-title">{post.title}</h5>
                            <h6 className="card-subtitle mb-6 text-muted post-author">{post.author}</h6>
                            <blockquote className="blockquote mb-0 post-text">{post.text}</blockquote>
                        </div>
                        <div className="card-footer">
                            <button className="btn btn-sm btn-outline-primary mr-2" onClick={handlePostEdit}>Edit</button>
                            <button className="btn btn-sm btn-outline-danger" onClick={handlePostDelete}>Delete</button>
                        </div>
                    </div>
                )
            }
        </>
    )
}