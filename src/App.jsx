import { Switch, Route } from 'react-router-dom';
import logo from './logo.svg';
import Main from './pages/Main';
import NavMenu from './pages/NavMenu';
import NewPost from './pages/NewPost';
import NotFound from './pages/NotFound';
import PostList from './pages/PostList';
import Posts from './pages/Posts';
//import './App.css';

function App() {
  return (
    <>
      <NavMenu></NavMenu>
      <div className="container">
        <Switch>
          <Route exact path="/" component={Main}></Route>
          <Route path="/posts" component={PostList}></Route>
          <Route path="/new" component={NewPost}></Route>
          <Route component={NotFound}></Route>
        </Switch>
      </div>
    </>
  );
}

export default App;
