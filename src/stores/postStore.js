import { EventEmitter } from 'events';
import dispatcher from '../appDispatcher';
import actionTypes from '../actions/actionTypes';

const CHANGE_POSTS_EVENT = 'change';
let _posts = [];

class PostStore extends EventEmitter {
    addChangeListener(callback) {
        this.on(CHANGE_POSTS_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_POSTS_EVENT, callback);
    }

    emitChangeEvent() {
        this.emit(CHANGE_POSTS_EVENT);
    }

    getPosts() {
        return _posts;
    }
}

const postStore = new PostStore();

dispatcher.register((action) => {
    console.log(action);
    switch (action.actionType) {
        case actionTypes.GET_POSTS:
            _posts = action.posts;
            postStore.emitChangeEvent();
            break;
        case actionTypes.ADD_POST:
            let maxId = _posts.reduce(
                (acc, val) => val.id > acc ? val.id : acc
                , 0);
            action.post.id = maxId + 1;
            _posts.push(action.post);
            postStore.emitChangeEvent();
            break;
        case actionTypes.UPDATE_POST:
            _posts
                .filter(t => t.id === action.post.id)
                .forEach(t => {
                    t.id = action.post.id;
                    t.title = action.post.title;
                    t.author = action.post.author;
                    t.text = action.post.text;
                });

            postStore.emitChangeEvent();
            console.log(_posts);
            break;
        case actionTypes.DELETE_POST:
            _posts = _posts.filter(t => t.id !== action.id)
            postStore.emitChangeEvent();

            console.log(_posts);
            break;
        default: ;
    }
});

export default postStore;