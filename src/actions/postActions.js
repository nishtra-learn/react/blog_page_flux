import dispatcher from '../appDispatcher';
import actionTypes from './actionTypes';
import data from '../db.json';

export function getPosts() {
    console.log('Request to API/file');
    dispatcher.dispatch({
        actionType: actionTypes.GET_POSTS,
        posts: data.posts
    })
}

export function addPost(post) {
    console.log('Add new post');
    dispatcher.dispatch(
        {
            actionType: actionTypes.ADD_POST,
            post: post
        }
    )
}

export function updatePost(post) {
    console.log(`Update post ID ${post.id}`);
    dispatcher.dispatch(
        {
            actionType: actionTypes.UPDATE_POST,
            post: post
        }
    )
}

export function deletePost(id) {
    console.log(`Delete post ID ${id}`);
    dispatcher.dispatch(
        {
            actionType: actionTypes.DELETE_POST,
            id: id
        }
    )
}